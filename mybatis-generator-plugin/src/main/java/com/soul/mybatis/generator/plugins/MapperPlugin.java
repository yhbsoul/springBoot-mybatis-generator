package com.soul.mybatis.generator.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.List;

/**
 * @author
 * @ClassNmae MapperPlugin
 * @create 2020-04-04 11:17
 **/
public class MapperPlugin extends PluginAdapter {

    private FullyQualifiedJavaType dataAnnotation;

    public MapperPlugin() {
        dataAnnotation = new FullyQualifiedJavaType("org.apache.ibatis.annotations.Mapper");
    }

    @Override
    public boolean validate(List<String> list) {
        return true;
    }

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        addDataAnnotation(interfaze);
        return true;
    }

    /**
     * Adds the @Data lombok import and annotation to the class
     *
     * @param topLevelClass
     */
    protected void addDataAnnotation(Interface topLevelClass) {
        topLevelClass.addImportedType(dataAnnotation);
        topLevelClass.addAnnotation("@Mapper");
    }
}
