# MyBatis Generator Lombok plugin and Comment and Mapper plugin

## 实现的功能

- 主要整合了lombok插件实现getter/setter等通用方法的自动生成，同时自定义实现了一个注释生成器，
通过抓取数据库表里面的注释作为实体类的注释内容。
- 其次整合了Mapper.java 中注释@Mapper，适用于springBoot基础SQL。

## 配置文件实现方式

同时添加配置文件`generatorConfig.xml`,使用的时候请根据项目需要自行修改对应配置

```xml

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">

<generatorConfiguration>

    <context id="MySqlTables" targetRuntime="MyBatis3" defaultModelType="flat">
        <property name="javaFileEncoding" value="UTF-8"/>

        <!-- 分页相关 -->
        <plugin type="org.mybatis.generator.plugins.RowBoundsPlugin"/>
        <!-- 自定义的注释生成插件-->
        <plugin type="com.soul.mybatis.generator.plugins.CommentPlugin">
            <!-- 抑制警告 -->
            <property name="suppressTypeWarnings" value="true"/>
            <!-- 是否去除自动生成的注释 true：是 ： false:否 -->
            <property name="suppressAllComments" value="true"/>
            <!-- 是否生成注释代时间戳-->
            <property name="suppressDate" value="true"/>
        </plugin>
        <plugin type="com.soul.mybatis.generator.plugins.LombokPlugin">
            <property name="hasLombok" value="true"/>
        </plugin>
        <plugin type="com.soul.mybatis.generator.plugins.MapperPlugin">
<!--            <property name="hasLombok" value="true"/>-->
        </plugin>

        <commentGenerator>
            <property name="suppressAllComments" value="true"/>
            <property name="suppressDate" value="true"/>
        </commentGenerator>

        <jdbcConnection driverClass="com.mysql.jdbc.Driver"
                        connectionURL="jdbc:mysql://localhost:3306/soul"
                        userId="root" password="123456"/>
        <!-- 默认false，把JDBC DECIMAL 和 NUMERIC 类型解析为 Integer，为 true时把JDBC DECIMAL 和
                   NUMERIC 类型解析为java.math.BigDecimal -->
        <javaTypeResolver>
            <property name="" value="false"/>
        </javaTypeResolver>

        <javaModelGenerator targetPackage="com.soul.single.entity" targetProject="src/main/java">
            <property name="enableSubPackages" value="false"/>
            <property name="rootClass" value="com.soul.single.entity.BaseEntity"/>
            <property name="trimStrings" value="true"/>
        </javaModelGenerator>

        <sqlMapGenerator targetPackage="mapper" targetProject="src/main/resources">
            <property name="enableSubPackages" value="false"/>
        </sqlMapGenerator>

        <!-- 对于mybatis来说，即生成Mapper接口，注意，如果没有配置该元素，那么默认不会生成Mapper接口
        targetPackage/targetProject:同javaModelGenerator
        type：选择怎么生成mapper接口（在MyBatis3/MyBatis3Simple下）：
            1，ANNOTATEDMAPPER：会生成使用Mapper接口+Annotation的方式创建（SQL生成在annotation中），不会生成对应的XML；
            2，MIXEDMAPPER：使用混合配置，会生成Mapper接口，并适当添加合适的Annotation，但是XML会生成在XML中；
            3，XMLMAPPER：会生成Mapper接口，接口完全依赖XML；
        注意，如果context是MyBatis3Simple：只支持ANNOTATEDMAPPER和XMLMAPPER
    -->
        <javaClientGenerator type="XMLMAPPER" targetPackage="com.soul.single.mapper" targetProject="src/main/java">
            <property name="enableSubPackages" value="false"/>
        </javaClientGenerator>


        <table tableName="SYS_USER" alias="SYS_USER"
               enableInsert="true" enableSelectByPrimaryKey="true"
               enableSelectByExample="false" enableUpdateByPrimaryKey="true"
               enableDeleteByPrimaryKey="true" enableDeleteByExample="false"
               enableCountByExample="false" enableUpdateByExample="false">
            <property name="useActualColumnNames" value="false"/>
        </table>

        <!-- 结束 -->
    </context>
</generatorConfiguration>
```


